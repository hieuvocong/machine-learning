# machine-learning

1. keras-demo: basic neural network for mnist

2. keras-cnn-mnist: simple cnn 2d for mnist

3. keras-cnn-imdb: cnn 1d for text classification (imdb)